package designpatterns.creational.factory;

public interface Currency {
	String getSymbol();
}
