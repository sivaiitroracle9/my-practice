package designpatterns.creational.abstractFactory;

public interface Animal {
	public void makeSound();
}
