package designpatterns.creational.abstractFactory;

public abstract class SpeciesFactory {
	public abstract Animal getAnimal(String animal);
}
